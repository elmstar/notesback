<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;


class NoteController extends Controller
{
	private $request;
	public function __construct()
	{
		$this->request = request();
	}

	public function noteList()
	{
		if ($this->request->sort_date)
			return Note::orderBy('created_at')->paginate(10)->toJson();
		elseif ($this->request->sort_favorite)
			return Note::orderBy('favorite', 'desc')->paginate(10)->toJson();
		else
			return Note::paginate(10)->toJson();
	}
	public function noteCreate()
	{
		$note = Note::create($this->request->all());
		return response()->json('ok');
	}
	public function noteEdit()
	{
		$note = Note::find($this->request->id);
		if ($note) {
			return response()->json($note);
		}

	}
	public function noteSave()
	{
		$note = Note::find($this->request->id);
		if ($note) {
			$note->update($this->request->all());
			return response()->json('ok');
		}  else {
			return response()->json('Что-то пошло не так');
		}

	}
	public function noteDelete()
	{
		$note = Note::find($this->request->id);
		if ($note) {
			$note->delete();
			return response()->json('ok');
		} else {
			return response()->json( $this->request->all());
		}

	}
	public function favorite()
	{
		$note = Note::find($this->request->id);
		if ($note) {
			$note->favorite = ($this->request->checked == 'true')?true:false;
			$note->update();
		}
		return response()->json($this->request->all());
	}
}
