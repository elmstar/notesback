<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\NoteController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [NoteController::class, 'noteList'])->name('notes_list');
Route::post('/create', [NoteController::class, 'noteCreate'])->name('notes_create');
Route::get('/edit', [NoteController::class, 'noteEdit'])->name('notes_edit');
Route::post('/edit', [NoteController::class, 'noteSave']);
Route::get('/delete', [NoteController::class, 'noteDelete'])->name('notes_delete');

Route::get('/favorite', [NoteController::class,'favorite']);
//Route::get('/get_token', [NoteController::class, 'get_token']);
Route::middleware('auth:sanctum')->get('/get_token', [NoteController::class, 'get_token']);