<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Notes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if (!Schema::hasTable('notes')) {
			Schema::create('notes', function (Blueprint $table)
			{
				$table->increments('id');
				$table->string('name', 128);
				$table->text('description')->nullable();
				$table->boolean('favorite')->default(false);
				$table->timestamps();
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('notes')) {
			Schema::drop('notes');
		}
    }
}
